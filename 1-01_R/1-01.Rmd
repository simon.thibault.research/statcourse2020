---
title: An R Markdown document converted from "1-01.ipynb"
output: html_document
---

# Base R
See also:
- https://www.statmethods.net/input/datatypes.html  
- https://www.r-bloggers.com/why-do-we-use-arrow-as-an-assignment-operator/

## Assignment

```{r}
# Create a variable and assign a value to it
a = 1

# We can also use an arrow for assignment
a0 <- 1

a
a0
a == a0
```

```{r}
# "a" is a numeric variable stored as a double
typeof(a)
```

```{r}
# We could force a to be stored as an integer
a_int = as.integer(a)

typeof(a_int)
```

## Common data types

### Character strings

```{r}
# Character string
b = "Bonjour"

b
typeof(b)
```

```{r}
# For character strings two types of quotation marks can be used interchangeably
d = 'Bonjour'

b == d
```

### Logicals

```{r}
# Logical / boolean
c = TRUE

c
typeof(c)
```

### Vectors

```{r}
# Vector
d = c(1,4,5) # vector

d
typeof(d)
```

```{r}
# For consecutive sequences, we can be lazy
d_seq = 2:10

d_seq
```

```{r}
# If we mix data types in a single vector, it will be all converted to a unique data type
e = c(pi,a_int,"Merci")

e
typeof(e)
```

```{r}
# We can refer to one element of a vector
e[1]

# And even to several ("slicing")
e[1:2]
e[c(1,3)]
```

### Lists

```{r}
# List
f = list(name = "Laura", mynumbers = d, age = 42)

f
typeof(f)
```

```{r}
# We can refer to one element of a list in different ways that do not produce the same output
f[1]
typeof(f[1])
```

```{r}
f$name
typeof(f$name)
```

```{r}
f["name"]
typeof(f["name"])
```

```{r}
f[["name"]]
typeof(f[["name"]])
```

```{r}
# We can also slice lists
f[1:2]
f[c(1,3)]
f[c("name","age")]
```

### Data frames

```{r}
# We can build dataframes, that look like tables

g <- c("Lou","Damien","Sarah")
h <- c(31, 29, 3)
i <- c("mother","father","child")

df <- data.frame(Name = g, Age = h, Role = i)

df
```

```{r}
names(df)
```

```{r}
# It's not a good idea to have capital letters in vaiable names, let's correct that
names(df) <- c("name","age","role")

names(df)
```

```{r}
# A quicker way to do that when e have many variables
names(df) <- tolower(names(df))

names(df)
```

```{r}
# Just like for lists, there are a variety of ways to select variables of a data frame

df[c(1,3)] # columns 3,4,5 of data frame
df[c("age","role")] # columns "age" and "role" from data frame
df$name # variable name in the data frame
```

```{r}
# We can also select specific rows
df[2,]
```

```{r}
# We can combine ow and column selection simultaneously
df[2,c("age","role")]
```

# Functions

## Calling a function

```{r}
dicenum = seq(from = 1,to = 6, by = 1)

y = sample(x = dicenum, size = 100)
```

```{r}
mean(y)
```

```{r}
dicenum = seq(1,6,1)

y = sample(dicenum, 100)

mean(y)
```

## Creating a function

# Importing data

```{r}
library(tidyverse)
```

```{r}
# Let's use the function that seems made for reading csv files
df <- read_csv("1-01_dummy-data.csv")

df
```

```{r}
df <- read_tsv("1-01_dummy-data.csv")

df
```

```{r}
print(df)
```

## Pipe

```{r}
df %>% print()
```

# Tidying data

## separate() and unite()

```{r}
df <- read_tsv("1-01_dummy-data_messy1.csv")

df
```

```{r}
df %>% separate(col = block, into = c("condition","block_num"), sep = "_")
```

How I got it messy in the first place:

```{r}
df %>% unite(col = block, condition, block_num, sep = "_")
# write_tsv(df, "1-01_dummy-data_messy-1.csv")
```

## gather() and spread()

```{r}
df <- read_tsv("1-01_dummy-data_messy2.csv")

df
```

```{r}
df  %>% gather(key = stim_side, value = response_time, L, R)
```

How I made it messy in the first place:

```{r}

# df <- df %>% separate(col = block, into = c("condition","block_num"), sep = "_")
df %>%
    select(subject,condition,stim_side,response_time) %>%
#     unite(col = tmp, condition, stim_side, sep="_") %>%
    group_by(subject, condition, stim_side) %>% summarise(response_time = mean(response_time)) %>% 
    spread(key = stim_side, value = response_time) %>%
    write_tsv("1-01_dummy-data_messy2.csv")
```

```{r}
df %>% select(subject,condition)
```

# Cleaning data

```{r}
df <- read_tsv("1-01_dummy-data.csv")
df
```

## Manipulating columns

### select()

```{r}
df %>% select(subject,condition,response_time)
```

```{r}
df %>% select(subject,condition,starts_with("response"))
```

```{r}
df %>% select_if(is.character)
```

### rename()

```{r}
df %>% rename(id = subject, difficulty = condition)
```

## Manipulating rows

### filter()

```{r}
df %>% filter(condition == "easy")
```

```{r}
df %>% filter(stim_time > 3)
```

### arrange()

```{r}
df %>% arrange(block_num, response_time)
```

## Managing missing values (NAs)

```{r}
df.na <- read_csv("1-01_dummy-data_NA.csv")

df.na %>% print()
```

### drop_na()

```{r}
df.na  %>% filter(!is.na(stim_time) & !is.na(response_time))
```

```{r}
df.na  %>% drop_na(any_of(c("stim_time","response_time")))
```

### replace_na()

```{r}
df.na
```

```{r}
df.na %>% 
    replace_na(list(condition = "easy", response = "R"))
```

# Transforming data

## mutate() and map()

```{r}
df %>% print()
```

```{r}
df %>% mutate(condition = as.factor(condition)) %>% print()
```

```{r}
df %>% mutate_if(is.character, as.factor) %>% print()
```

```{r}
df %>% mutate(reaction_time = response_time - stim_time) %>% select(-block_num) %>% print(width = Inf)
```

```{r}
df %>% mutate(stim_time = map2_dbl(stim_side, stim_time,
                                   .f=function(ss,st){if (ss=="L") {st} else {st-1}})) 
```

## group_by() and summarise()

```{r}
df.rt <- df %>% mutate(reaction_time = response_time - stim_time) %>% select(-block_num)

df.rt %>% summarise(mean_rt = mean(reaction_time))
```

```{r}
df.rt <- df.rt %>% rename(rt = reaction_time)

# We can summarise multiple variables at once
df.rt %>% summarise(mean_rt = mean(rt),
                    sd_rt = sd(rt))
```

```{r}
# Summarised variable exist immediately
df.rt %>% summarise(mean_rt = mean(rt),
                    sd_rt = sd(rt),
                    z = mean_rt/sd_rt)
```

```{r}
# We can summarise within subsets defined by levels of factors
df.rt %>%
    group_by(subject,condition) %>% 
    summarise(mean_rt = mean(rt),
              sd_rt = sd(rt))
```

```{r}
# We can do complicated stuff with group_by, e.g. outlier detection by levels of factors 
df.rt %>%
    group_by(subject,condition) %>% 
    filter(rt < mean(rt) + sd(rt)) %>%
    summarise(mean_rt = mean(rt),
              sd_rt = sd(rt))
```

```{r}
df.rt %>%
    group_by(subject,condition) %>% 
    mutate(kept = rt < mean(rt) + sd(rt))
```

## recode_factor()

```{r}
# We can change how levels of a factor are coded
df %>%
    mutate_if(is.character,as.factor) %>%
    mutate(condition = recode_factor(condition, "easy"="E","hard"="H")) %>% print()
```

# Combining data frames

## bind_rows()

```{r}
df.Anna <- read_tsv("1-01_dummy-data_Anna.csv") %>% print()
df.Boris <- read_tsv("1-01_dummy-data_Boris.csv") %>% print()
```

```{r}
bind_rows(df.Anna, df.Boris)
```

## *_join()

```{r}
df.tmp <- df %>%
    rownames_to_column(var = "trial_num") %>% # print()
    group_by(subject) %>% 
    mutate(trial_num = rank(trial_num))

df.tmp %>% select(subject,trial_num,block_num,condition) %>% write_tsv("1-01_dummy-data_factors.csv")

df.tmp %>% select(subject,trial_num,stim_time,response_time) %>% write_tsv("1-01_dummy-data_times.csv")
```

```{r}
df.factors <- read_tsv("1-01_dummy-data_factors.csv") %>% arrange(subject,trial_num)
df.factors

df.times <- read_tsv("1-01_dummy-data_times.csv") %>% arrange(subject,trial_num)
df.times
```

```{r}
full_join(df.factors, df.times)
```

# Dta visualization

```{r}
df <- read_tsv("1-01_data_participants.csv")

df
```

**Convert this notebook to R markdown**

```{r}
library(rmarkdown)
input = "1-01.ipynb"
rmarkdown:::convert_ipynb(input, output = xfun::with_ext(input, "Rmd"))
```

